var deferredPromt;

if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('/sw.js')
        .then(function() {
            console.log('Service worker registered!');
        })
        .catch((err) => {
            console.log(err);
        });
}

window.addEventListener('beforeinstallpromt', function(event) {
    console.log('beforeinstallpromt fired', event);
    event.preventDefault();
    deferredPromt = event;
    return false;
});

var promise = new Promise((resolve, reject) => {
    setTimeout( () => {
        // resolve('timer is done');
        reject({code: 500, message: 'An error occured'});       
    }, 3000);
});

fetch('https://httpbin.org/ip')
    .then((response) => {
        console.log(response);
        return response.json();
    })
    .then((data) => {
        console.log(data);
        
    })
    .catch((err) => {
        console.log(err);
    });

fetch('https://httpbin.org/post', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        mode: 'cors',
        body: JSON.stringify({
            message: 'Does this work?'
        })
    })
    .then((response) => {
        console.log(response);
        return response.json();
    })
    .then((data) => {
        console.log(data);
        
    })
    .catch((err) => {
        console.log(err);
    });

promise
      .then((text) => {
        return text;
    }).then((newText) => {
        console.log(newText);
    }).catch((err) => {
        console.log(err.code, err.message)
    });

console.log('after timeout');